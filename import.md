# Aufgabe

Diese Dokumentation wird auf folgende Punkte eingehen: 
- Datenstruktur
- Vorraussetzungen zur Integration
- Gründe zur Wahl von couchbase
- Einrichtung von couchbase
- Umwandlung der daten
- Import der Daten

Hierbei soll ein Überblick auf die mit der Datenbank im Hinblick auf die Aufgabe verbundene Arbeit gegeben werden.

# Datenstruktur

Die gegebenen Daten enthalten zusammenhängende Einträge, wie Sie in einer Schule oder Universität gefunden werden könnten.
Wie in der folgenden Abbildung gezeigt, sind Menschen, Kurse und benötigte sowie bezahlte Materialien abgebildet.

![Datenstruktur](structure.png)

Ersichtlich ist, dass viele Fremdschlüssel genutzt werden, um Verknüpfungen zwischen Einträgen herzustellen.

# Vorraussetzungen zur Integration

Es ist eine Datenbank von nöten, die alle vorliegenden Tabellenstrukturen als Dokumente speichern kann.
Aus dem vorherigen Kapitel geht desweiteren hervor, dass mittels einer JOIN-ähnlichen Operation miteinander verknüpfte Dokumente geladen werden können müssen.

# Gründe zur Wahl von couchbase

Unsere Wahl ist hierbei auf Couchbase gefallen, da damit eine gründlich entwickelte, gut getestete und leicht skalierbare Dokumenten-datenbank vorliegt.
Dolumente können also verschiedenste Formen annehmen, und erfüllen damit unsere vorhergehende Forderung nach verschiedene "Tabellenformaten".
Desweiteren können dank "SQL++", der Sprache zur Abfrage und Modifikation von Dokumenten, per JOIN-Anweisung miteinander verknüpfte Dokumente geladen und modifiziert werden.

# Einrichtung von couchbase

Wir haben uns zur Einrichtung von Couchbase für Docker entschieden, da hier schon ein passendes Image zur Verfügung steht.
Grundsätzlich kann die Anleitung befolgt werden, die hier zu finden ist: https://docs.couchbase.com/server/current/install/getting-started-docker.html

Wir sind nicht von dieser Anleitung abgewichen, und haben die Standardwerte zum Resourcenverbrauch nicht verändert.

Über die URL "http://localhost:8091" ist die Instanz in einerm Browser erreichbar.

Username: vboxuser PW: changeme

Um nun alle daten einpflegen zu können, wurde der Bucket "hs" und der Scope "nosql" angelegt. Dies erlaubt eine Abgrenzung der Daten von anderen Daten.

Für jede Tabelle aus den Quelldaten wurde nun eine Collection angelegt, die die Tabellenzeilen als Dokumente halten wird.

# Umwandlung der daten

Couchbase unterstützt pro Dokument nur einen Key. Hier war es also essentiell, die richtigen Spalten aus den Quelldaten zu einem Key zusammenzufassen.
Gleichzeitig mussten die Daten aus der Form eines SQL-dumps in eine key-value Form gebracht werden, wobei value in unserem Fall ausschließlich DOkumente waren.
Wollen wir also den folgenden Text in eine passende Form bringen, müssen wir ein entsprechendes Regex erstellen:
```
insert into Angebot values(1, 'G08', '13.10.2023', 'Wedel');
insert into Angebot values(2, 'G08', '24.11.2023', 'Ulm');
insert into Angebot values(1, 'G10', '01.12.2023', 'Augsburg');
insert into Angebot values(2, 'G10', '15.02.2023', 'Muenchen');
insert into Angebot values(1, 'P13', '28.05.2023', 'Augsburg');
insert into Angebot values(2, 'P13', '01.07.2023', 'Augsburg');
insert into Angebot values(1, 'I09', '27.03.2023', 'Mindelheim');
insert into Angebot values(2, 'I09', '23.04.2023', 'Muenchen');
insert into Angebot values(3, 'I09', '29.05.2023', 'Ulm');
```
Mithilfe des Suchmusters

```insert into Angebot values\((\d+), '([^']+)', '([^']+)', '([^']+)'\);```

bekommen wir die Werte aus der Tabelle in capture-groups.
Durch das replace-pattern 

```insert into offer (KEY, VALUE) VALUES ("$1-$2", {"date": "$3", "city": "$4"});```

Erzeugen wir folgenden SQL++ code, der für Couchbase verständlich ist:
```
insert into offer (KEY, VALUE) VALUES ("1-G08", {"date": "13.10.2023", "city": "Wedel"})
insert into offer (KEY, VALUE) VALUES ("2-G08", {"date": "24.11.2023", "city": "Ulm"})
insert into offer (KEY, VALUE) VALUES ("1-G10", {"date": "01.12.2023", "city": "Augsburg"})
insert into offer (KEY, VALUE) VALUES ("2-G10", {"date": "15.02.2023", "city": "Muenchen"})
insert into offer (KEY, VALUE) VALUES ("1-P13", {"date": "28.05.2023", "city": "Augsburg"})
insert into offer (KEY, VALUE) VALUES ("2-P13", {"date": "01.07.2023", "city": "Augsburg"})
insert into offer (KEY, VALUE) VALUES ("1-I09", {"date": "27.03.2023", "city": "Mindelheim"})
insert into offer (KEY, VALUE) VALUES ("2-I09", {"date": "23.04.2023", "city": "Muenchen"})
insert into offer (KEY, VALUE) VALUES ("3-I09", {"date": "29.05.2023", "city": "Ulm"})
```
Hier sollte angemerkt werden, dass es bessere Ansätze zur Konvertierung gibt.
Regex würde z.b. Schwierigkeiten bekommen, sobald die Daten anführungszeichen enthalten.

Bei größeren/komplexeren Datenmengen würde sich ein Program eher anbeiten, welches die Daten konvertiert und einspielt. Hier ein sinnbildliches Beispiel:
```
def transfer_table(table):
    entries = table.get_all()
    key = f'{entries["AngNr"]-{entries["KursNr"]}}'
    couchbase.create_document(table.name, key, entries)
```

# Import der Daten
Die im vorhergehenden Kapitel erzeugten SQL++ Befehle können nun über die Oberfläche von Couchbase ausgeführt, und damit die Collections befüllt werden.